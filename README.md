# Pagar.Me Wrapper
## Criação do gateway
```php
$pagarMe = new PagarMe\Gateway\PagarMe(
    $apiKey
);
```

## Obtenção da chave pública
```php
$publicKeyRequest = new PagarMe\Gateway\PublicKey\Request($encryptionKey);
$publicKey = $pagarMe->publicKey($publicKeyRequest);
```
## Criação do gateway
```php
$creditCard = new PagarMe\Gateway\CreditCard\Request(
    4539962793379259, 
    'Eduardo',
    '0519',
    629,
    $publicKey
);
```
## Realizando uma transação com cartão de crédito
```php
$card = $pagarMe->card($creditCard);
$request = new PagarMe\Gateway\Transaction\Request(3000, $card);
$transaction = $pagarMe->transaction($request);
```

## Estornando uma transação
```php
$request = new PagarMe\Gateway\Refund\Request($transaction);
$refund = $pagarMe->refund($request);
var_dump($refund);
```