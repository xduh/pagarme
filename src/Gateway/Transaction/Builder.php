<?php

namespace PagarMe\Gateway\Transaction;

use PagarMe\Gateway\CreditCard\Builder as CreditCardBuilder;

class Builder
{
    public static function create($data)
    {
        $transaction = new Transaction();
        $transaction->setId($data->id);
        $transaction->setStatus($data->status);
        $transaction->setRefuseReason($data->refuse_reason);
        $transaction->setStatusReason($data->status_reason);
        $transaction->setAcquirerResponseCode($data->acquirer_response_code);
        $transaction->setAcquirerName($data->acquirer_name);
        $transaction->setAuthorizationCode($data->authorization_code);
        $transaction->setSoftDescriptor($data->soft_descriptor);
        $transaction->setTid($data->tid);
        $transaction->setNsu($data->nsu);
        $transaction->setDateCreated($data->date_created);
        $transaction->setDateUpdated($data->date_updated);
        $transaction->setAmount($data->amount);
        $transaction->setAuthorizedAmount($data->authorized_amount);
        $transaction->setPaidAmount($data->paid_amount);
        $transaction->setRefundedAmount($data->refunded_amount);
        $transaction->setInstallments($data->installments);
        $transaction->setCost($data->cost);
        $transaction->setPostbackURL($data->postback_url);
        $transaction->setPaymentMethod($data->payment_method);
        $transaction->setCaptureMethod($data->capture_method);
        $transaction->setReferer($data->referer);
        $transaction->setIp($data->ip);
        $transaction->setMetadata($data->metadata);

        if (is_object($data->card)) {
            $transaction->setCard(CreditCardBuilder::create($data->card));
        }

        return $transaction;
    }
}
