<?php

namespace PagarMe\Gateway\Transaction;

use PagarMe\Gateway\Request as RequestInterface;
use PagarMe\Gateway\CreditCard\CreditCard;

class Request implements RequestInterface
{
    const ENDPOINT = 'transactions';
    const METHOD = 'POST';

    private $amount;
    private $card;

    public function __construct($amount, CreditCard $card)
    {
        $this->amount   = $amount;
        $this->card = $card;
    }

    public function getMethod()
    {
        return self::METHOD;
    }

    public function getEndpoint()
    {
        return self::ENDPOINT;
    }

    public function getBody()
    {
        return [
            'amount'  => $this->amount,
            'card_id' => $this->card->getId()
        ];
    }
}
