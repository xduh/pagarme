<?php

namespace PagarMe\Gateway\Transaction;

use PagarMe\Gateway\CreditCard\CreditCard;

class Transaction
{
    private $id;
    private $card;
    private $status;
    private $refuseReason;
    private $statusReason;
    private $acquirerResponseCode;
    private $acquirerName;
    private $authorizationCode;
    private $softDescriptor;
    private $tid;
    private $nsu;
    private $dateCreated;
    private $dateUpdated;
    private $amount;
    private $authorizedAmount;
    private $paidAmount;
    private $refundedAmount;
    private $installments;
    private $cost;
    private $postbackURL;
    private $paymentMethod;
    private $captureMethod;
    private $referer;
    private $ip;
    private $metadata;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCard()
    {
        return $this->card;
    }

    public function setCard(CreditCard $card)
    {
        $this->card = $card;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getRefuseReason()
    {
        return $this->refuseReason;
    }

    public function setRefuseReason($refuseReason)
    {
        $this->refuseReason = $refuseReason;
    }

    public function getStatusReason()
    {
        return $this->statusReason;
    }

    public function setStatusReason($statusReason)
    {
        $this->statusReason = $statusReason;
    }

    public function getAcquirerResponseCode()
    {
        return $this->acquirerResponseCode;
    }

    public function setAcquirerResponseCode($acquirerResponseCode)
    {
        $this->acquirerResponseCode = $acquirerResponseCode;
    }

    public function getAcquirerName()
    {
        return $this->acquirerName;
    }

    public function setAcquirerName($acquirerName)
    {
        $this->acquirerName = $acquirerName;
    }

    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorizationCode = $authorizationCode;
    }

    public function getSoftDescriptor()
    {
        return $this->softDescriptor;
    }

    public function setSoftDescriptor($softDescriptor)
    {
        $this->softDescriptor = $softDescriptor;
    }

    public function getTid()
    {
        return $this->tid;
    }

    public function setTid($tid)
    {
        $this->tid = $tid;
    }

    public function getNsu()
    {
        return $this->nsu;
    }

    public function setNsu($nsu)
    {
        $this->nsu = $nsu;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getAuthorizedAmount()
    {
        return $this->authorizedAmount;
    }

    public function setAuthorizedAmount($authorizedAmount)
    {
        $this->authorizedAmount = $authorizedAmount;
    }

    public function getPaidAmount()
    {
        return $this->paidAmount;
    }

    public function setPaidAmount($paidAmount)
    {
        $this->paidAmount = $paidAmount;
    }

    public function getRefundedAmount()
    {
        return $this->refundedAmount;
    }

    public function setRefundedAmount($refundedAmount)
    {
        $this->refundedAmount = $refundedAmount;
    }

    public function getInstallments()
    {
        return $this->installments;
    }

    public function setInstallments($installments)
    {
        $this->installments = $installments;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getPostbackURL()
    {
        return $this->postbackURL;
    }

    public function setPostbackURL($postbackURL)
    {
        $this->postbackURL = $postbackURL;
    }

    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    public function getCaptureMethod()
    {
        return $this->captureMethod;
    }

    public function setCaptureMethod($captureMethod)
    {
        $this->captureMethod = $captureMethod;
    }

    public function getReferer()
    {
        return $this->referer;
    }

    public function setReferer($referer)
    {
        $this->referer = $referer;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    public function getMetadata()
    {
        return $this->metadata;
    }

    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }
}
