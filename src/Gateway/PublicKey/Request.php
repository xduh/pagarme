<?php

namespace PagarMe\Gateway\PublicKey;

use PagarMe\Gateway\Request as RequestInterface;

class Request implements RequestInterface
{
    const ENDPOINT = 'transactions/card_hash_key';
    const METHOD = 'GET';

    private $encryptionKey;

    public function __construct($encryptionKey)
    {
        $this->encryptionKey = $encryptionKey;
    }

    public function getEndpoint()
    {
        return self::ENDPOINT;
    }

    public function getMethod()
    {
        return self::METHOD;
    }

    public function getBody()
    {
        return ['encryption_key' => $this->encryptionKey];
    }
}
