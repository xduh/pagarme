<?php

namespace PagarMe\Gateway\PublicKey;

class PublicKey
{
    private $dateCreated;
    private $id;
    private $ip;
    private $value;

    public function __construct($dateCreated, $id, $ip, $value)
    {
        $this->dateCreated = $dateCreated;
        $this->id          = $id;
        $this->ip          = $ip;
        $this->value       = $value;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function getValue()
    {
        return $this->value;
    }
}
