<?php

namespace PagarMe\Gateway;

use GuzzleHttp\Client;
use PagarMe\Gateway\Transaction\Request as TransactionRequest;
use PagarMe\Gateway\Refund\Request as RefundRequest;
use PagarMe\Gateway\CreditCard\Request as CreditCardRequest;
use PagarMe\Gateway\PublicKey\Request as PublicKeyRequest;
use PagarMe\Gateway\PublicKey\PublicKey;
use PagarMe\Gateway\CreditCard\CreditCard;
use PagarMe\Gateway\Request;
use GuzzleHttp\Exception\BadResponseException;
use PagarMe\Gateway\Transaction\Builder as TransactionBuilder;
use PagarMe\Gateway\CreditCard\Builder as CreditCardBuilder;
use PagarMe\Gateway\Refund\Builder as RefundBuilder;

class PagarMe
{
    const API_BASE = 'https://api.pagar.me/1/';
    const TIMEOUT = 5;

    private $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;

        $this->client = new Client([
            'base_uri' => self::API_BASE,
            'timeout'  => self::TIMEOUT,
        ]);
    }

    public function transaction(TransactionRequest $transaction)
    {
        $response = $this->send($transaction);
        return TransactionBuilder::create($response);
    }

    public function publicKey(PublicKeyRequest $publicKey)
    {
        $response = $this->send($publicKey);
        return new PublicKey(
            $response->date_created,
            $response->id,
            $response->ip,
            $response->public_key
        );
    }

    public function card(CreditCardRequest $card)
    {
        $response = $this->send($card);
        return CreditCardBuilder::create($response);
    }

    public function refund(RefundRequest $refund)
    {
        $response = $this->send($refund);
        return RefundBuilder::create($response);
    }

    private function send(Request $request)
    {
        try {
            $response = $this->client->request(
                $request->getMethod(),
                $request->getEndpoint(),
                [
                    'json' => $this->buildBody($request)
                ]
            );

            return json_decode($response->getBody()->getContents());
        } catch (BadResponseException $e) {
            throw new PagarMeException($e->getMessage());
        }
    }

    private function buildBody(Request $request)
    {
        return array_merge(
            $request->getBody(),
            ['api_key' => $this->apiKey]
        );
    }
}
