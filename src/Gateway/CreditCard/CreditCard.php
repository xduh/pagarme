<?php

namespace PagarMe\Gateway\CreditCard;

class CreditCard
{
    private $id;
    private $dateCreated;
    private $dateUpdated;
    private $brand;
    private $holderName;
    private $firstDigits;
    private $lastDigits;
    private $country;
    private $fingerprint;
    private $customer;
    private $valid;
    private $expirationDate;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    public function getHolderName()
    {
        return $this->holderName;
    }

    public function setHolderName($holderName)
    {
        $this->holderName = $holderName;
    }

    public function getFirstDigits()
    {
        return $this->firstDigits;
    }

    public function setFirstDigits($firstDigits)
    {
        $this->firstDigits = $firstDigits;
    }

    public function getLastDigits()
    {
        return $this->lastDigits;
    }

    public function setLastDigits($lastDigits)
    {
        $this->lastDigits = $lastDigits;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getFingerprint()
    {
        return $this->fingerprint;
    }

    public function setFingerprint($fingerprint)
    {
        $this->fingerprint = $fingerprint;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    public function getValid()
    {
        return $this->valid;
    }

    public function setValid($valid)
    {
        $this->valid = $valid;
    }

    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }
}
