<?php

namespace PagarMe\Gateway\CreditCard;

use PagarMe\Gateway\Request as RequestInterface;
use PagarMe\Gateway\PublicKey\PublicKey;

class Request implements RequestInterface
{
    const ENDPOINT = 'cards';
    const METHOD = 'POST';

    private $cardHash;
    private $cardId;

    public function __construct(
        $number,
        $holder,
        $expiration,
        $cvv,
        PublicKey $publicKey
    ) {
        $this->generateCardHash(
            $number,
            $holder,
            $expiration,
            $cvv,
            $publicKey->getValue(),
            $publicKey->getId()
        );

        $this->cardId = $cardId;
    }

    public function getEndpoint()
    {
        return self::ENDPOINT;
    }

    public function getMethod()
    {
        return self::METHOD;
    }

    public function getBody()
    {
        return ['card_hash' => $this->cardHash];
    }

    private function generateCardHash(
        $number,
        $holder,
        $expiration,
        $cvv,
        $publicKey,
        $id
    ) {

        $rawParams = array(
            "card_number"          => $number,
            "card_holder_name"     => $holder,
            "card_expiration_date" => $expiration,
            "card_cvv"             => $cvv,
        );

        $formattedParams = [];

        foreach ($rawParams as $key => $value) {
            $formattedParams[]= sprintf('%s=%s', $key, $value);
        }

        $querystring = implode('&', $formattedParams);

        $openKey = openssl_get_publickey($publicKey);

        openssl_public_encrypt($querystring, $encrypt, $openKey);

        $this->cardHash = sprintf('%s_%s', $id, base64_encode($encrypt));
    }
}
