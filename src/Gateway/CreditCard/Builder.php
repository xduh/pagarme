<?php

namespace PagarMe\Gateway\CreditCard;

class Builder
{
    public static function create($data)
    {
        $creditCard = new CreditCard();

        $creditCard->setId($data->id);
        $creditCard->setDateCreated($data->date_created);
        $creditCard->setDateUpdated($data->date_updated);
        $creditCard->setBrand($data->brand);
        $creditCard->setHolderName($data->holder_name);
        $creditCard->setFirstDigits($data->first_digits);
        $creditCard->setLastDigits($data->last_digits);
        $creditCard->setCountry($data->country);
        $creditCard->setFingerprint($data->fingerprint);
        $creditCard->setCustomer($data->customer);
        $creditCard->setValid($data->valid);
        $creditCard->setExpirationDate($data->expiration_date);

        return $creditCard;
    }
}
