<?php

namespace PagarMe\Gateway\Refund;

use PagarMe\Gateway\CreditCard\CreditCard;

class Refund
{
    private $id;
    private $status;
    private $refuseReason;
    private $statusReason;
    private $acquirerResponseCode;
    private $acquirerName;
    private $authorizationCode;
    private $softDescriptor;
    private $tid;
    private $nsu;
    private $dateCreated;
    private $dateUpdated;
    private $amount;
    private $authorizedAmount;
    private $paidAmount;
    private $refundedAmount;
    private $installments;
    private $cost;
    private $postbackURL;
    private $paymentMethod;
    private $referer;
    private $ip;
    private $card;
    private $splitRules;
    private $metadata;

    public function getStatus()
    {
        return $status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getRefuseReason()
    {
        return $refuseReason;
    }

    public function setRefuseReason($refuseReason)
    {
        $this->refuseReason = $refuseReason;
    }

    public function getStatusReason()
    {
        return $statusReason;
    }

    public function setStatusReason($statusReason)
    {
        $this->statusReason = $statusReason;
    }

    public function getAcquirerResponseCode()
    {
        return $acquirerResponseCode;
    }

    public function setAcquirerResponseCode($acquirerResponseCode)
    {
        $this->acquirerResponseCode = $acquirerResponseCode;
    }

    public function getAcquirerName()
    {
        return $acquirerName;
    }

    public function setAcquirerName($acquirerName)
    {
        $this->acquirerName = $acquirerName;
    }

    public function getAuthorizationCode()
    {
        return $authorizationCode;
    }

    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorizationCode = $authorizationCode;
    }

    public function getSoftDescriptor()
    {
        return $softDescriptor;
    }

    public function setSoftDescriptor($softDescriptor)
    {
        $this->softDescriptor = $softDescriptor;
    }

    public function getTid()
    {
        return $tid;
    }

    public function setTid($tid)
    {
        $this->tid = $tid;
    }

    public function getNsu()
    {
        return $nsu;
    }

    public function setNsu($nsu)
    {
        $this->nsu = $nsu;
    }

    public function getDateCreated()
    {
        return $dateCreated;
    }

    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    public function getDateUpdated()
    {
        return $dateUpdated;
    }

    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    public function getAmount()
    {
        return $amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getAuthorizedAmount()
    {
        return $authorizedAmount;
    }

    public function setAuthorizedAmount($authorizedAmount)
    {
        $this->authorizedAmount = $authorizedAmount;
    }

    public function getPaidAmount()
    {
        return $paidAmount;
    }

    public function setPaidAmount($paidAmount)
    {
        $this->paidAmount = $paidAmount;
    }

    public function getRefundedAmount()
    {
        return $refundedAmount;
    }

    public function setRefundedAmount($refundedAmount)
    {
        $this->refundedAmount = $refundedAmount;
    }

    public function getInstallments()
    {
        return $installments;
    }

    public function setInstallments($installments)
    {
        $this->installments = $installments;
    }

    public function getId()
    {
        return $id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCost()
    {
        return $cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function getPostbackURL()
    {
        return $postbackURL;
    }

    public function setPostbackURL($postbackURL)
    {
        $this->postbackURL = $postbackURL;
    }

    public function getPaymentMethod()
    {
        return $paymentMethod;
    }

    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    public function getReferer()
    {
        return $referer;
    }

    public function setReferer($referer)
    {
        $this->referer = $referer;
    }

    public function getIp()
    {
        return $ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    public function getCard()
    {
        return $card;
    }

    public function setCard(CreditCard $card)
    {
        $this->card = $card;
    }

    public function getSplitRules()
    {
        return $splitRules;
    }

    public function setSplitRules($splitRules)
    {
        $this->splitRules = $splitRules;
    }

    public function getMetadata()
    {
        return $metadata;
    }

    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }
}
