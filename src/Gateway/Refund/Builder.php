<?php

namespace PagarMe\Gateway\Refund;

use PagarMe\Gateway\CreditCard\Builder as CreditCardBuilder;

class Builder
{
    public static function create($data)
    {
        $refund = new Refund();

        $refund->setStatus($data->status);
        $refund->setRefuseReason($data->refuse_reason);
        $refund->setStatusReason($data->status_reason);
        $refund->setAcquirerResponseCode($data->acquirer_response_code);
        $refund->setAcquirerName($data->acquirer_name);
        $refund->setAuthorizationCode($data->authorization_code);
        $refund->setSoftDescriptor($data->soft_descriptor);
        $refund->setTid($data->tid);
        $refund->setNsu($data->nsu);
        $refund->setDateCreated($data->date_created);
        $refund->setDateUpdated($data->date_updated);
        $refund->setAmount($data->amount);
        $refund->setAuthorizedAmount($data->authorized_amount);
        $refund->setPaidAmount($data->paid_amount);
        $refund->setRefundedAmount($data->refunded_amount);
        $refund->setInstallments($data->installments);
        $refund->setId($data->id);
        $refund->setCost($data->cost);
        $refund->setPostbackURL($data->postback_url);
        $refund->setPaymentMethod($data->payment_method);
        $refund->setReferer($data->referer);
        $refund->setIp($data->ip);
        $refund->setSplitRules($data->split_rules);
        $refund->setMetadata($data->metadata);

        if (is_object($data->card)) {
            $refund->setCard(CreditCardBuilder::create($data->card));
        }

        return $refund;
    }
}
