<?php

namespace PagarMe\Gateway\Refund;

use PagarMe\Gateway\Request as RequestInterface;
use PagarMe\Gateway\Transaction\Transaction;

class Request implements RequestInterface
{
    const ENDPOINT = 'transactions/%d/refund';
    const METHOD = 'POST';

    private $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    public function getMethod()
    {
        return self::METHOD;
    }

    public function getEndpoint()
    {
        return sprintf(self::ENDPOINT, $this->transaction->getId());
    }

    public function getBody()
    {
        return [];
    }
}
