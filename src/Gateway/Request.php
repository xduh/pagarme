<?php

namespace PagarMe\Gateway;

interface Request
{
    public function getMethod();
    public function getEndpoint();
    public function getBody();
}
