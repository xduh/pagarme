<?php

namespace PagarMe\GatewayTest\Transaction;

use PagarMe\Gateway\Transaction\Request;

class RequestTest extends \PHPUnit_Framework_TestCase
{
    /**
    * @test
    */
    public function shouldReturnValidBody()
    {
        $creditCardMock = $this->getMockBuilder('PagarMe\Gateway\CreditCard\CreditCard')
            ->disableOriginalConstructor()
            ->getMock();
        $creditCardMock->method('getId')
            ->willReturn('card_ci6l9fx8f0042rt16rtb477gj');

        $request = new Request(50000, $creditCardMock);

        $this->assertEquals(
            [
                'amount' => 50000,
                'card_id' => 'card_ci6l9fx8f0042rt16rtb477gj',
            ],
            $request->getBody()
        );
    }
}
