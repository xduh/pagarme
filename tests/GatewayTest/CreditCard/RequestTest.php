<?php

namespace PagarMe\GatewayTest\CreditCard;

use PagarMe\Gateway\CreditCard\Request;

class RequestTest extends \PHPUnit_Framework_TestCase
{
    const PUBLIC_KEY = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCceQZxG9GFm63zz3PTsXLf93qd
kJZ2H9XoHWE+sxhKtrY1hRO62g3KXKUniM3vbhXDGI0jkowJAhtpJ1LzG/vTTsS6
u8gSTV+Et7C5seZJ2ZZ07RopYR4F3NvGyUutVbcTXO9us1++CBjGE0WAjS2hd61R
9hI9aU8DtzYE1Xo5kwIDAQAB
-----END PUBLIC KEY-----';

    const EXPECTED_HASH = '123_KwQ/GEWBeYvFJDpCAR0fNutRjIFzGjrxWNkqVmlh/38yA5X8w6SyEX6oBnXcyV3tzeIEVBZEfWIlrrA3M4VJjitbeSN+qk2pSqzTzGXiypNUyy1j+jCSrYdkvi8k0daKwTu5O0OjoGVJqFhHT0xMuajCyHWnjpUBTHLgBMQTEaM=';
    /**
    * @test
    */
    public function shouldReturnValidBody()
    {
        $publicKeyMock = $this->getMockBuilder('PagarMe\Gateway\PublicKey\PublicKey')
            ->disableOriginalConstructor()
            ->getMock();
        $publicKeyMock->method('getValue')->willReturn(self::PUBLIC_KEY);
        $publicKeyMock->method('getId')->willReturn(12345);


        $request = new Request(
            4539962793379259,
            'Eduardo',
            '0519',
            629,
            $publicKeyMock
        );

        $body = $request->getBody();
        $this->assertArrayHasKey('card_hash', $body);
        $this->assertRegExp('/[0-9]+_[a-zA-Z0-9\/\+]+=/', $body['card_hash']);
    }
}
