<?php

namespace PagarMe\GatewayTest\Refund;

use PagarMe\Gateway\Refund\Request;

class RequestTest extends \PHPUnit_Framework_TestCase
{
    /**
    * @test
    */
    public function shouldReturnValidEndpoint()
    {
        $TransactionMock = $this->getMockBuilder('PagarMe\Gateway\Transaction\Transaction')
            ->disableOriginalConstructor()
            ->getMock();
        $TransactionMock->method('getId')
            ->willReturn('1337');

        $request = new Request($TransactionMock);

        $this->assertEquals(
            'transactions/1337/refund',
            $request->getEndpoint()
        );
    }
}
