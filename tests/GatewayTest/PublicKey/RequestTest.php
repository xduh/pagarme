<?php

namespace PagarMe\GatewayTest\PublicKey;

use PagarMe\Gateway\PublicKey\Request;

class RequestTest extends \PHPUnit_Framework_TestCase
{
    /**
    * @test
    */
    public function shouldReturnValidBody()
    {
        $request = new Request('ek_test_q5vZGIkmbWJHcdYbN8NatCFaEuWSQX');

        $this->assertEquals(
           ['encryption_key' => 'ek_test_q5vZGIkmbWJHcdYbN8NatCFaEuWSQX'],
            $request->getBody()
        );
    }
}
